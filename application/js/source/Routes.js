/*// configure routes
// url Templates: js/components/{Component}/Templates/{Templatename}.html

App.config(function($routeProvider) {

    var basePath = 'js/components/';

    $routeProvider
        
    
        .when('/', {
            templateUrl : basePath + 'AppComponent/Templates/AppTemplate.html',
            //controller  : 'mainController'
        })

        
        .when('/user/authenticate', {
            templateUrl : basePath + 'UserComponent/Templates/AuthenticationTemplate.html',
            //controller  : 'AuthenticationController'
        })


        .when('/user/registration', {
            templateUrl : basePath + 'UserComponent/Templates/RegistrationTemplate.html',
            //controller  : 'RegistrationController'
        })

        .otherwise({
            redirectTo: '/404'
        });
});*/

// configure routes
// url Templates: js/components/{Component}/Templates/{Templatename}.html

var basePath = 'js/source/templates/';

window.routes =
{
    "/": {
        templateUrl : basePath + 'AppTemplate.html',
        requireLogin: false
    },
    "/evenement/checkin": {
        templateUrl : basePath + 'AuthenticationTemplate.html',
        requireLogin: false
    },
    "/evenement/gebruiker/zoek": {
        templateUrl : basePath + 'EventSearchUserTemplate.html',
        requireLogin: false
    },
    "/evenement/gebruiker/nieuw": {
        templateUrl : basePath + 'RegistrationTemplate.html',
        requireLogin: false
    },
    "/toog/bestelling/nieuw/:callback?": {
        templateUrl : basePath + 'NewOrderTemplate.html',
        requireLogin: false
    },
    "/toog/bestelling/overzicht": {
        templateUrl : basePath + 'OrderSummaryTemplate.html',
        requireLogin: false
    },
    "/beheer": {
        templateUrl : basePath + 'AdminDashboardTemplate.html',
        requireLogin: false
    },
    "/beheer/evenementen": {
        templateUrl : basePath + 'EventsTemplate.html',
        requireLogin: false
    },
    "/beheer/evenement/nieuw": {
        templateUrl : basePath + 'NewEventTemplate.html',
        requireLogin: false
    },
    "/beheer/evenement/:id": {
        templateUrl : basePath + 'EventDetailTemplate.html',
        requireLogin: false
    },
    "/beheer/leden": {
        templateUrl : basePath + 'AllMembersTemplate.html',
        requireLogin: false
    },
    "/beheer/leden/:id": {
        templateUrl : basePath + 'MemberTemplate.html',
        requireLogin: false
    },
};

App.config(function($routeProvider, RestangularProvider) {

    RestangularProvider.setBaseUrl(App.serviceUrl + '/sjatoo-symfony/web/api/v1');

    //this loads up our routes dynamically from the previous object 
    for(var path in window.routes) {
        $routeProvider.when(path, window.routes[path]);
    }
    $routeProvider
    .otherwise({
        templateUrl : basePath + '404Template.html',
        requireLogin: false
    });
    
})
.run(function($rootScope, $location) {

    // $rootScope.$on("$locationChangeStart", function(event, next, current) {
     
    //     //var urlArray = next.split("index.html#");
    //     var urlArray = next.split("#");
    //     var routeName = urlArray[1];

    //     if(window.routes[routeName].requireLogin == true) {
    //         if(UserService.isUserAuthenticated() != true) {
    //             $location.path('login');
    //             //event.preventDefault();            
    //         }
    //     }
    // });

});