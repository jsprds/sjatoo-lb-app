(function() {

    angular.module('sjatoo').controller('UserListController', [
        '$scope',
        'Restangular',
        'UserService',
        function ($scope, Restangular, UserService) {

        // Constructor
        var init = function() {
            console.log('UserListController loaded');

            $scope.users = [];
            loadAllUsers(25,0);
        };

        // controller functions
        var loadAllUsers = function(limit, offset) {

            UserService.getMemberList(limit, offset).then(function(users) {

                for(var i = 0, l = users.length; i < l; i++) {
                    $scope.users.push(users[i]);
                }

                var l = $scope.users.length;

                if(l == limit) {
                    loadAllUsers(l + 25, l);
                }

            },function() {
                console.error('error');
            });
                
        }
        
        // CALL INIT        
        init();
    }]);

})();