(function() {

    // App
    var app = angular.module('sjatoo', ['ngResource', 'ngRoute', 'ui.bootstrap', 'restangular']);

    // config
    app.config(function($routeProvider, $httpProvider) {

        var basePath = 'js/source/templates/';

        var routes = {
            "/": {
                templateUrl : basePath + 'AppTemplate.html',
                requireLogin: false
            },
            "/evenement/checkin": {
                templateUrl : basePath + 'AuthenticationTemplate.html',
                requireLogin: false
            },
            "/evenement/gebruiker/zoek": {
                templateUrl : basePath + 'EventSearchUserTemplate.html',
                requireLogin: false
            },
            "/evenement/gebruiker/nieuw": {
                templateUrl : basePath + 'RegistrationTemplate.html',
                requireLogin: false
            },
            "/toog/bestelling/nieuw/:callback?": {
                templateUrl : 'js/source/OrderComponent/NewOrderTemplate.html',
                requireLogin: false
            },
            "/toog/bestelling/overzicht": {
                templateUrl : 'js/source/OrderComponent/OrderSummaryTemplate.html',
                requireLogin: false
            },
            "/beheer": {
                templateUrl : basePath + 'AdminDashboardTemplate.html',
                requireLogin: false
            },
            "/beheer/evenementen": {
                templateUrl : basePath + 'EventsTemplate.html',
                requireLogin: false
            },
            "/beheer/evenement/nieuw": {
                templateUrl : basePath + 'NewEventTemplate.html',
                requireLogin: false
            },
            "/beheer/evenement/:id": {
                templateUrl : basePath + 'EventDetailTemplate.html',
                requireLogin: false
            },
            "/beheer/leden": {
                templateUrl : 'js/source/UserComponent/UserListTemplate.html',
                requireLogin: false
            },
            "/beheer/leden/:id": {
                templateUrl : basePath + 'MemberTemplate.html',
                requireLogin: false
            },
        };

        // this loads up our routes dynamically from the previous object 
        for(var path in routes) {
            $routeProvider.when(path, routes[path]);
        }

        $routeProvider.otherwise({
            templateUrl : basePath + '404Template.html',
            requireLogin: false
        });

    });

    angular.module('sjatoo').run(function($http) {
        
        $http.defaults.headers.common.Authorization = 'Bearer ZDlmMTFjYjVmNDU0YzNjYjY3MjIyOTFiZjk2NWFkNDVhYTliMjhlNmI1OTllYjBjZjFjYTUzNzUxY2VjODRkNA' ;
      
    });

})();