App.controller('EventsController', [
    '$scope',
    '$location',
    'Restangular',
    'EventService',
    function ($scope, $location, Restangular, EventService) {

    /*
    Stap1: functie Init
    --------------------
    */
    var init = function() {
        console.log('EventsController started!');

        // get all events
        getAllEvents();
    };

    /*
    Stap2: Controller vars (niet in scope!)
    ------------------------------------------
    */

    /*
    Stap3: Controller functions (niet in scope!)
    --------------------------------------------
    */
    var getAllEvents = function() {

        Restangular.all('events').getList()
        .then(function onSuccess(events) {

            for(var i =0; i < events.length; i++) {
                EventService.addEvent(events[i]);
            }

            console.log(EventService.getCurrentEvents());

            $scope.events = EventService.getCurrentEvents();
            
        }, function onFail(data) {

            console.log('Er liep iets mis: ' + data.message);

        });
    }


    /*
    Stap4: Scope vars
    ------------------
    */
    $scope.events;
    $scope.error;
   
    /* Stap5: Scope functions
    -------------------------
    */
    $scope.selectEvent = function(event) {
        EventService.setSelectedEvent(event); 
        $location.path('/evenement/' + event.id);
    }
  
    /* Stap6: init aanroepen
    --------------------
    */
    // start deze motherfucking controller! BAM!
    init();

}]);