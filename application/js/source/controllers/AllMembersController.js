App.controller('AllMembersController', [
    '$scope',
    'Restangular',
    'UserService',
    function ($scope, Restangular, UserService) {

    /*
    Stap1: functie Init
    --------------------
    */
    var init = function() {
        console.log('AllMembersController started!');

        getAllMembers(25,0);
    };

    /*
    Stap2: Controller vars (niet in scope!)
    ------------------------------------------
    */
    var getAllMembers = function(limit, offset) {

        Restangular.all('users').getList({
            'limit': limit,
            'offset': offset
        })
        .then(function onSuccess(members) {

            for(var i = 0; i < members.length; i++) {
                UserService.addUser(members[i]);
            }

            $scope.members = UserService.getUsers();
            
        }, function onFail(data) {

            console.log('Er liep iets mis: ' + data.message);

        });
    }

    /*
    Stap3: Controller functions (niet in scope!)
    --------------------------------------------
    */

    /*
    Stap4: Scope vars
    ------------------
    */
    $scope.members;
   
    /* Stap5: Scope functions
    -------------------------
    */
    $scope.load = function() {
        getAllMembers(25, UserService.getUsers().length);
    }
  
    /* Stap6: init aanroepen
    --------------------
    */
    // start deze motherfucking controller! BAM!
    init();
    
}]);