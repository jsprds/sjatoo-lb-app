App.controller('EventDetailController', [
    '$scope',
    'EventService',
    function ($scope, EventService) {

    /*
    Stap1: functie Init
    --------------------
    */
    var init = function() {
        console.log('EventsController started!');

        if(EventService.getSelectedEvent()) { 
            console.log('Er is een event in de cache');
        }
        else {
            console.log('Geen event in de cache');
        }
    };

    /*
    Stap2: Controller vars (niet in scope!)
    ------------------------------------------
    */

    /*
    Stap3: Controller functions (niet in scope!)
    --------------------------------------------
    */
    var getAllEvents = function() {

        EventService.getAllEvents()
        .$promise
        .then(function onSuccess(data) {
            
            if(data.status == '200') {
                for(var i =0; i < data.data.length; i++) {
                    EventService.addCurrentEvent(data.data[i]);
                }

                $scope.events = EventService.getCurrentEvents();
            }
            else if(data.status == '404') {
                $scope.error = 'Deze kaart is nog niet geregistreerd.';
            } 
            else {
                console.log('Er liep iets mis: ' + data.message);
            } 
        }, function onFail(data) {
            //internet lag eruit, backend lag plat, service is niet bereikt...
            console.log('er ging iets mis', data);
        })
        .finally(function () {

        });
    }


    /*
    Stap4: Scope vars
    ------------------
    */
    $scope.events;
    $scope.error;
   
    /* Stap5: Scope functions
    -------------------------
    */
  
    /* Stap6: init aanroepen
    --------------------
    */
    // start deze motherfucking controller! BAM!
    init();

}]);