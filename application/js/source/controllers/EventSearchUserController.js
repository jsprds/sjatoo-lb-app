App.controller('EventSearchUserController', [
    '$scope',
    'MemberService',
    function ($scope, MemberService) {

    /*
    Stap1: functie Init
    --------------------
    */

    // hier kan je zaken doen die ALTIJD moeten gebeuren bij het inladen van deze pagina, bijvoorbeeld: config ophalen, user checken en ophalen, een of andere call naar service met info... getNumberOfRegisterdUsers ofzo...

    var init = function() {
        console.log('EventSearchUserController loaded');

        calculateDisplayYears();
    };

    /*
    Stap2: Controller vars (niet in scope!)
    ------------------------------------------
    */
    var date;

    /*
    Stap3: Controller functions (niet in scope!)
    --------------------------------------------
    */
    var calculateDisplayYears = function() {
        var startYear = new Date().getFullYear() - 18;
        for(var i = startYear - 8; i < startYear + 4; i++) {
            $scope.years[$scope.years.length] = i;
        }
    }

    var searchUserByDateOfBirth = function(date) {
        MemberService.getMemberByDateOfBirth({'date': date})
        .$promise 
        .then(function onSuccess(data) {

            if(data.status == '200') {

                // Gegevens gevonden!
                $scope.members = data.data.members;

            }
            else if(data.status == '404'){

                // Request gelukt maar geen gegevens gevonden
                console.log(data.status);
                console.log('Er liep iets mis: ' + data.message);

            } 
            else {
                console.log(data.status);
                console.log('Er liep iets mis: ' + data.message);
            }

        }, 
        function onFail(data) {
            //internet lag eruit, backend lag plat, service is niet bereikt...
            console.log('er ging iets mis', data);
        })
        .finally(function () {
            //dit wordt na elke call aangesproken, mislukt of niet... handig om bijvoorbeeld je button terug te enablen of een loading-icoontje te verbergen
        });        
    }

    /*
    Stap4: Scope vars
    ------------------
    */
    $scope.user = {};
    $scope.error;
    $scope.event = {};
    
    $scope.years = [];
    $scope.months = []
    $scope.days = [];

    /* Stap5: Scope functions
    -------------------------
    */

    $scope.addToYears = function(i) {
        if(i == '-1') {
            $scope.years.splice(0, 0, $scope.years[0] - 1);
            $scope.years.splice($scope.years.length - 1, 1);
        }
        else {
            $scope.years.push($scope.years[$scope.years.length - 1] + 1);
            $scope.years.splice(0, 1);
        }
    }

    $scope.selectYear = function(year) {
        $scope.years = [];
        date = year;

        $scope.months = [{name: "Jan", value: 1}
                        ,{name: "Feb", value: 2}
                        ,{name: "Maa", value: 3}
                        ,{name: "Apr", value: 4}
                        ,{name: "Mei", value: 5}
                        ,{name: "Jun", value: 6}
                        ,{name: "Jul", value: 7}
                        ,{name: "Aug", value: 8}
                        ,{name: "Sep", value: 9}
                        ,{name: "Okt", value: 10}
                        ,{name: "Nov", value: 11}
                        ,{name: "Dec", value: 12}];
    }

    $scope.selectMonth = function(month) {
        $scope.months = [];
        date += '-' + month;

        $scope.days = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
    }

    $scope.selectDay = function(day) {
        $scope.days = [];
        date += '-' + day;

        searchUserByDateOfBirth(date);
    }

    /* Stap6: init aanroepen
    --------------------
    */
    // start deze motherfucking controller! BAM!
    init();
}]);