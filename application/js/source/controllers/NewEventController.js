App.controller('NewEventController', [
    '$scope',
    '$location',
    'EventService',
    function ($scope, $location, EventService) {

    /*
    Stap1: functie Init
    --------------------
    */
    var init = function() {
        console.log('NewEventController started!');

    };

    /*
    Stap2: Controller vars (niet in scope!)
    ------------------------------------------
    */

    /*
    Stap3: Controller functions (niet in scope!)
    --------------------------------------------
    */

    /*
    Stap4: Scope vars
    ------------------
    */
    $scope.event = {};
   
    /* Stap5: Scope functions
    -------------------------
    */
    $scope.saveEvent = function(event) {

        EventService.addEvent({'event' : event})
        .$promise
        .then(function onSuccess(data) {
            
            if(data.status == '200') {
                console.log('gelukt');
            }
            else {
                console.log('Er liep iets mis: ' + data.message);
            } 

        }, function onFail(data) {
            //internet lag eruit, backend lag plat, service is niet bereikt...
            console.log('er ging iets mis', data);
        })
        .finally(function () {
            // Zet scanner terug op actief
            $timeout(function() { // wacht 2 seconden voor scanner terug actief zodat er niet peronguluk 2x gescanned wordt
                $scope.listener = true;
                scanRfid();
            }, 2000);
        });
    }
  
    /* Stap6: init aanroepen
    --------------------
    */
    // start deze motherfucking controller! BAM!
    init();

}]);