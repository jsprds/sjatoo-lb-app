App.controller('AdminDashBoardController', [
    '$scope',
    function ($scope) {

    /*
    Stap1: functie Init
    --------------------
    */
    var init = function() {
        console.log('AdminDashBoardController started!');
    };

    /*
    Stap2: Controller vars (niet in scope!)
    ------------------------------------------
    */

    /*
    Stap3: Controller functions (niet in scope!)
    --------------------------------------------
    */

    /*
    Stap4: Scope vars
    ------------------
    */
   
    /* Stap5: Scope functions
    -------------------------
    */
  
    /* Stap6: init aanroepen
    --------------------
    */
    // start deze motherfucking controller! BAM!
    init();
    
}]);