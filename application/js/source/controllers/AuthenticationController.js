App.controller('AuthenticationController', [
    '$scope',
    '$timeout',
    '$http',
    '$modal',
    'Restangular',
    'MemberService',
    'RfidService',
    'EidService',
    'EventService',
    function ($scope, $timeout, $http, $modal, Restangular, MemberService, RfidService, EidService, EventService) {

    /*
    Stap1: functie Init
    --------------------
    */
    // hier kan je zaken doen die ALTIJD moeten gebeuren bij het inladen van deze pagina, bijvoorbeeld: config ophalen, user checken en ophalen, een of andere call naar service met info... getNumberOfRegisterdUsers ofzo...

    var init = function() {
        console.log('AuthenticationController started')
        scanRfid();
        getEvent();
    };

    /*
    Stap2: Controller vars (niet in scope!)
    ------------------------------------------
    */
    var progressBarWidth = 100;
    var progressBarInterval;
    var eventCheckTime; // wanneer is er voor het laatst gecheckt of er een event bezig is?

    /*
    Stap3: Controller functions (niet in scope!)
    --------------------------------------------
    */

    // Reset Screen
    var reset = function() {
        $scope.member = null; // MemberService.user = {}; werkt niet
        $scope.listener = false;
        $scope.error = ""; // Reset errors

        resetProgressBar();
        if(typeof pauseInterval !== 'undefined') {
            clearInterval(pauseInterval);
        }
    }

    var resetProgressBar = function() {
        console.log('resetProgressBar');
        var progressBar = $('#progressBar');
        progressBarWidth = 100;
        clearInterval(progressBarInterval);
        progressBar.width(progressBarWidth + '%');
    }

    // Functie die de scanner op actief zet
    var scanRfid = function() {
        RfidService.listenToRfidReader().then(function(card) { 
            if(eventRefreshTime() >= 10) { // als het minstens x minuut(en) geleden is dat we op een event hebben gezocht doen we dit nog eens
                getEvent();
            }

            reset(); // Reset alle vars uit vorige scan
            getMember(card); // Nieuwe data
            
        });
    };

    // API call naar user met rfid ..
    var getMember = function(rfid) {

        Restangular.one('rfids', rfid).get()
        .then(function onSuccess(user) {

            // Status 200 -> Alles liep goed, we hebben een user gevonden
            MemberService.setCurrentMember(user);
            $scope.member = MemberService.getCurrentMember();

            //misschien nog enkele checks doen voor checkin?
            //Check1: lidkaart vervallen?
            if(new Date($scope.member.rfidExpireDate) < new Date().getTime()) {

                console.log('Expired!!');

                // Lidkaart is vervallen --> Toon scherm
                $scope.member.rfidExpired = true;

                var modalInstance = $modal.open({
                    controller: 'MembershipExpiredModalController',
                    templateUrl: 'js/source/templates/MemberShipExpiredModal.html'
                });

                modalInstance.result.then(function (result) {
                    // Closed --> OK --> CHECKIN
                    console.log(result)
                    console.log('Verleng --> Toon Datum');
                    d = new Date();
                    d.setFullYear(d.getFullYear() + 1);
                    $scope.member.rfidExpires = d.toISOString().slice(0, 19).replace('T', ' ');
                    console.log($scope.member);
                    console.log('checkin');
                }, function (result) {
                    // Dismissed --> NOK
                    console.log(result)
                });
            }
            else {
                // Alles is geldig --> Checkin member
                checkIn($scope.member);

                // laat de progressbar lopen!
                progressBar(); 
            }    
                 
        }, function onFail(error) {
            if(error.status == '404') {
                $scope.error = 'Deze kaart is nog niet geregistreerd.';
            }
            else {
                $scope.error = 'Er liep iets mis. Maar ik weet niet wat.';
            } 
        })
        .finally(function () {
            // Zet scanner terug op actief
            $timeout(function() { // wacht 2 seconden voor scanner terug actief zodat er niet peronguluk 2x gescanned wordt
                $scope.listener = true;
                scanRfid();
            }, 3000);
        });   
    }


    // Check de member in op het event
    var checkIn = function(member) {

        var data = {
            "checkinDate": new Date().toJSON(),
            "user": member.id
        };

        Restangular.all('checkins').post(data)
        .then(function onSuccess(data) {

            console.log($scope.member);

            $scope.member.checkedIn = Date.now();
            
        }, function onFail(data) {

            console.log(data);
            $scope.error = 'Er liep iets mis, check-in niet geregistreerd';

        });
    }

    // Verleng lidkaart
    var renewMembership = function(member) {
        MemberService.updateMember({'member': member})
        .$promise //we maken van deze call een promise, letterlijk een 'belofte', die ingelost moet worden, of net niet
        .then(function onSuccess(data) {
            //belofte gehouden, call is gelukt, wil enkel zeggen dat de service bereikt is, het kan dus nog altijd zijn dat de backend een fuckup doet en verkeerde data teruggeeft!
            //todo: check incoming data
            console.log('call naar service geslaagd', data);
            if(data.status == '200') {
                $scope.member.checkedIn = Date.now();;                
            }
            else {
                console.log('Er liep iets mis: ' + data.message);
                $scope.error = 'Er liep iets mis, check-in niet geregistreerd';
            } 
        }, function onFail(data) {
            //internet lag eruit, backend lag plat, service is niet bereikt...
            console.log('er ging iets mis', data);
            $scope.error = 'Er liep iets mis, check-in niet geregistreerd';
        })
        .finally(function () {
            //dit wordt na elke call aangesproken, mislukt of niet... handig om bijvoorbeeld je button terug te enablen of een loading-icoontje te verbergen
        });
    }
    
    var getEvent = function() {

        // Check of we al een event in het systeem hebben
        var currentEvents = EventService.getCurrentEvents();
        if(currentEvents) {
            $scope.event = EventService.getCurrentEvents()[0];
        }

        eventCheckTime = new Date();

        Restangular.all('events.json').getList({"date": new Date().toJSON(), "limit": 1})
        .then(function onSuccess(event) {

            if(event[0]) {
                eventCheckTime = new Date();
                EventService.addEvent(event[0]);
                $scope.event = EventService.getCurrentEvents()[0];
            }
           
            
        }, function onFail(data) {

            console.log('fail');

        });   

    }

    var eventRefreshTime = function() {
        d = new Date();
        if(eventCheckTime.getMinutes() <= d.getMinutes()) {
            return d.getMinutes() - eventCheckTime.getMinutes();
        }
        else {
            return (60 - eventCheckTime.getMinutes()) + d.getMinutes();
        }
    }

    /* Deze progressbar geeft het aantal seconden (20) dat de member te zien is na het scannen. */
    var progressBar = function() {
        var progressBar = $('#progressBar');

        if(progressBarInterval) {
            clearInterval(progressBarInterval);
        }
        
        progressBarInterval = setInterval(function() {

            progressBarWidth -= 5;

            progressBar.css('width', progressBarWidth + '%');

            if (progressBarWidth <= 0) {
                $scope.$apply(function() {
                    $scope.member = null; // MemberService.member = {}; werkt niet
                });

                clearInterval(progressBarInterval);
                resetProgressBar();
            }
        }, 1000)
    };

    var pauseProgressBar = function() {
        if(typeof progressBarInterval !== 'undefined') {
            if(typeof pauseInterval !== 'undefined') {
                clearInterval(pauseInterval);
            }

            var i = 0;
            clearInterval(progressBarInterval);
            pauseInterval = setInterval(function() {
                i += 2;
                console.log(i);

                if(i > 10) {
                    progressBar();
                    clearInterval(pauseInterval);
                }
                
            }, 1000)
        } 
    }

    var ISODateString = function(d) {
      function pad(n){return n<10 ? '0'+n : n}
      return d.getFullYear()+'-'
          + pad(d.getMonth()+1)+'-'
          + pad(d.getDate()) +' '
          + pad(d.getHours())+':'
          + pad(d.getMinutes())+':'
          + pad(d.getSeconds())
    }

    /*
    Stap4: Scope vars
    ------------------
    */
    $scope.member;
    $scope.error;
    $scope.event;
    $scope.listener = true; //rfidlistener active?

    /* Stap5: Scope functions
    -------------------------
    */

    // Indien het lidmaatschap vervallen is, is er de optie om niet te verlengen --> cancel
    $scope.cancel = function() {
        reset();
        $modalInstance.dismiss('cancel');
        console.log('click');
    }

    // Indien het lidmaatschap vervallen is, is er de optie om niet te verlengen --> cancel
    $scope.renewMembership = function() {
        console.log('click');
        // Expire date herschrijven in DB
        $modalInstance.close();
        $scope.rfidExpired = false;
    }

    $scope.memberOptions = function() {
        pauseProgressBar();
    }

    /* Stap6: init aanroepen
    --------------------
    */
    // start deze motherfucking controller! BAM!
    init();
}]);