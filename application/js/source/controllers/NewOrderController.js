App.controller('NewOrderController', [
    '$scope',
    '$routeParams',
    '$location',
    'Restangular',
    'OrderService',
    function ($scope, $routeParams, $location, Restangular, OrderService) {

    /*
    Stap1: functie Init
    --------------------
    */
    var init = function() {
        console.log('NewOrderController started!');

        apiRequestProductCategories();
        requestProductsFromApi();
    };

    /*
    Stap2: Controller vars (niet in scope!)
    ------------------------------------------
    */
    var callback = $routeParams.callback;

    /*
    Stap3: Controller functions (niet in scope!)
    --------------------------------------------
    */
    var requestProductsFromApi = function() {
        Restangular.all('products.json').getList()
        .then(function onSuccess(products) {

            $scope.products = products;
            
        }, function onFail(data) {

            console.log('fail');

        });
    }

    var apiRequestProductCategories = function() {
        Restangular.all('productcategories').getList()
        .then(function onSuccess(productCategories) {

            $scope.productCategories = productCategories;
            
        }, function onFail(data) {

            console.log('fail');

        });
    }

    var apiSaveOrder = function(order) {
        Restangular.all('orders').post(order)
        .then(function onSuccess(order) {

            console.log('success');

            if(callback == 'summary') {
                OrderService.addOrder(order);
                $location.path('/toog/bestelling/overzicht');
            }
            
        }, function onFail(data) {

            console.log('fail');

        });
    }

    /*
    Stap4: Scope vars
    ------------------
    */

    // ADDTOORDER //
    $scope.order = {
        products: []
    };
    
    /* Stap5: Scope functions
    -------------------------
    */

     // FILTER PRODUCTS LIST //
    $scope.filter = {category: {id: 1}};

    $scope.filterList = function(filter) {
        $scope.filter.category.id = filter;
    }

    // Amount functions
    $scope.riseAmount = function(product) {
        $scope.order.products[product].amount += 1;
    }

    // todo: mag niet kleiner worden als 0 en moet uit lijst bij < 1
    $scope.lowerAmount = function(product) {
        $scope.order.products[product].amount -= 1;

        if($scope.order.products[product].amount < 1) {
            $scope.order.products.splice(product, 1);
        }
    }

    // add product to order Object
    $scope.addToOrder = function(product) {
        //console.log(product);

        var indexOfProduct = productInArray(product, $scope.order.products);
        if(indexOfProduct >= 0) {
                $scope.order.products[indexOfProduct].amount += 1;
        }
        else {
            $scope.order.products.push({product: product, amount: 1});
        }       
    }

    $scope.confirmOrder = function(products) {

        var order = {};
        order.orderDate = new Date();
        order.orderDetails = [];

        for(var i = 0; i < products.length; i++){
            order.orderDetails[i] = {};
            order.orderDetails[i].product = products[i].product.id;
            order.orderDetails[i].amount = products[i].amount;
            order.orderDetails[i].price = products[i].product.price;
        }

        apiSaveOrder(order);

        $scope.order = {
            products: []
        };
    }

    // misschien beter op te lossen met underscore.js --> Nakijken
    var productInArray = function(search, array) {
        for(var i = 0; i < array.length; i++) {

            // abstracter te maken
           if(array[i].product.id === search.id) {
                return i;
           }
        }
        return -1;
    }
  
    /* Stap6: init aanroepen
    --------------------
    */
    // start deze motherfucking controller! BAM!
    init();

}]);