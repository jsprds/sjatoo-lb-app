App.controller('RegistrationController', [
    '$scope',
    '$modal',
    'Restangular',
    'EidService',
    'MemberService',
    'RfidService',
    function ($scope, $modal, Restangular, EidService, MemberService, RfidService) {

    /*
    Stap1: functie Init
    --------------------
    */

    // hier kan je zaken doen die ALTIJD moeten gebeuren bij het inladen van deze pagina, bijvoorbeeld: config ophalen, member checken en ophalen, een of andere call naar service met info... getNumberOfRegisterdmembers ofzo...

    var init = function() {
        console.log('RegistrationController loaded');

        //getStatusEidReader();
    };

    /*
    Stap2: Controller vars (niet in scope!)
    ------------------------------------------
    */
    var getStatusEidReader = function() {
        EidService.getCard()
        .$promise
        .then(function onSuccess(data) {

            console.log(data);

            if(data.status == 200) {
                $scope.eidReader.status = true;
                $scope.eidReader.message = 'EidReader is online en er is een kaart gevonden, lees data.';
            }
            else {
                $scope.eidReader.status = true;
                $scope.eidReader.message = 'EidReader is online maar geen kaart';
            }

        }, function onFail(data) {
            console.log('Fail');
            console.log(data);   
            $scope.eidReader.status = false;
            $scope.eidReader.message = 'EidReader is niet online, check alle kabels en EidServer';
        })
        .finally(function () {
            // $scope.loading = false;
        });
    };

    var checkIn = function(member) {

        var data = {
            "checkinDate": new Date().toJSON(),
            "user": member.id
        };

        Restangular.all('checkins').post(data)
        .then(function onSuccess(data) {

            $scope.member.checkedIn = Date.now();
            console.log(data);
           
            
        }, function onFail(data) {

            console.log(data);
            $scope.error = 'Er liep iets mis, check-in niet geregistreerd';

        });
    }

    var saveMember = function(member) {
        $scope.loading = true;

        Restangular.all('users').post(member)
        .then(function onSuccess(member) {
            
            $scope.saveMember.status = true;
            $scope.saveMember.message = 'De lidkaart voor ' + $scope.member.firstname + ' is geregistreerd';

            $scope.member = null;

            checkIn(member);
       
            
        }, function onFail(data) {

            $scope.saveMember.status = false;
            $scope.saveMember.message = data.data[0].message + ' scan een nieuwe kaart';
            //console.log(data.data[0].message);
            scanRfid();


            /*if(data.status == 409) {

                // Status 409 zonder duplicatedMember? --> Deze RFID kaart is al geregistreerd.
                if(!data.data) {
                    $scope.saveMember.status = false;
                    $scope.saveMember.message = 'Deze RFID kaart kan niet gebruikt worden. Markeren en aan de kant leggen voor onderzoek.';
                    $scope.member.RFID =  null;
                    scanRfid();
                }
                else {

                    // We denken dat er een gebruiker is die zich dubbel wil registreren --> Koppel nieuwe rfid kaart aan gebruiker
                    var modalInstance = $modal.open({
                        controller: 'DuplicatedMemberModalController',
                        templateUrl: 'js/source/templates/DuplicatedMemberModal.html',
                        resolve: {
                            response: function() {
                                var response = {};
                                response.duplicatedMember = data.data.duplicatedMember;
                                response.title = 'Oeps, Sjatoo kent ' + data.data.duplicatedMember.firstname + ' al.';

                                return response;
                            }
                        }
                    });

                    modalInstance.result.then(function (result) {
                        // Closed --> OK --> CHECKIN
                        console.log(result)
                    }, function (result) {
                        // Dismissed --> NOK
                        console.log(result)
                    }); 
                }
            }
            else {
                $scope.saveMember.status = false;
                $scope.saveMember.message = 'Er liep iets mis bij het opslaan in de database';
                console.log('onbekende fout');
            }*/
        })
        .finally(function () {
            $scope.loading = false;
        });
    };


    /*
    Stap3: Controller functions (niet in scope!)
    --------------------------------------------
    */
    // Functie die de scanner op actief zet
    var scanRfid = function() {
        $scope.rfidListener = true;
        RfidService.listenToRfidReader().then(function(card) { 
            console.log(card);
            $scope.member.rfid = card;
            $scope.rfidListener = false;
            saveMember($scope.member);
        });
    };

    var eidDateOfBirth = function(eidDate) {

        //eidDate format: 18 FEB 1993
        eidDateParts = eidDate.split(" ");

        //console.log(eidDateParts[1]);

        var month;

        switch(eidDateParts[1]) {
            case "JAN":
                month = '01';
                break;
            case "FEB":
                console.log('FEB 2');
                month = '02';
                break;
            case "MAA":
                console.log('MAA 3');
                month = '03';
                break;
            case "APR":
                console.log('APR 4');
                month = '04';
                break;
            case "MEI":
                console.log('MEI 5');
                month = '05';
                break;
            case "JUN":
                console.log('JUN 6');
                month = '06';
                break;
            case "JUL":
                console.log('JUL 7');
                month = '07';
                break;
            case "AUG":
                console.log('AUG 8');
                month = '08';
                break;
            case "SEP":
                console.log('SEP 9');
                month = '09';
                break;
            case "OKT":
                console.log('OKT 10');
                month = '10';
                break;
            case "NOV":
                console.log('NOV 11');
                month = '11';
                break;
            case "DEC":
                console.log('DEC 12');
                month = '12';
                break;
            default:
                console.log('FOUT');
        }

        return eidDateParts[3] + "/" + month + "/" + eidDateParts[0];
    }

    /*
    Stap4: Scope vars
    ------------------
    */
    $scope.eidReader = {};
    $scope.saveMember = {};
    $scope.member;
    $scope.loading = false;
    $scope.rfidListener = false;

    /* Stap5: Scope functions
    -------------------------
    */
    $scope.readEidCard = function() {
        console.log('readEidCard');
        $scope.saveMember = {};
        $scope.loading = true;

        EidService.getCard()
        .$promise
        .then(function onSuccess(data) {
           

            console.log(data);

            if(data.status == 200) {

                //data.data.dateOfBirth = eidDateOfBirth(data.data.dateOfBirth);

                // IN SOCKET!
                if((indexOf = data.data.firstname.indexOf(' ')) != -1) {
                    data.data.firstname = data.data.firstname.substr(0,indexOf);
                    console.log(data.data.firstname);
                }
                data.data.street =  data.data.address;
                data.data.zipcode =  data.data.zipCode;
                var d = new Date();
                d.setFullYear(d.getFullYear() + 1)
                data.data.rfidExpireDate = d;

                $scope.member = data.data;

                scanRfid();
            }
            else {
                console.log('Fail');
                $scope.eidReader.status = false;
                $scope.eidReader.message = 'Geen kaart';
            }

        }, function onFail(data) {
            console.log('Fail');   
            $scope.eidReader.status = false;
            $scope.eidReader.message = 'EidReader is niet online, check alle kabels en EidServer';
        })
        .finally(function () {
            $scope.loading = false;
        });
    }


    /* Stap6: init aanroepen
    --------------------
    */
    // start deze motherfucking controller! BAM!
    init();
}]);