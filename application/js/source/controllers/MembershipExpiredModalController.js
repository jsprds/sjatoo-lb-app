App.controller('MembershipExpiredModalController', [
    '$scope',
    '$modalInstance',
    function ($scope, $modalInstance) {

    /*
    Stap1: functie Init
    --------------------
    */

    // hier kan je zaken doen die ALTIJD moeten gebeuren bij het inladen van deze pagina, bijvoorbeeld: config ophalen, user checken en ophalen, een of andere call naar service met info... getNumberOfRegisterdUsers ofzo...
    var init = function() {
        console.log('MembershipExpiredModalController loaded');
        console.log($modalInstance);
    };

    /*
    Stap2: Controller vars (niet in scope!)
    ------------------------------------------
    */


    /*
    Stap3: Controller functions (niet in scope!)
    --------------------------------------------
    */

    /*
    Stap4: Scope vars
    ------------------
    */

    // Indien het lidmaatschap vervallen is, is er de optie om niet te verlengen --> cancel
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
        console.log('click');
    }

    // Indien het lidmaatschap vervallen is, is er de optie om niet te verlengen --> cancel
    $scope.renewMembership = function() {
        console.log('click');
        // Expire date herschrijven in DB
        $modalInstance.close('renew');
    }
    /* Stap6: init aanroepen
    --------------------
    */
    // start deze motherfucking controller! BAM!
    init();
}]);