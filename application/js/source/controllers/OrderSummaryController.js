App.controller('OrderSummaryController', [
    '$scope',
    '$timeout',
    'Restangular',
    'OrderService',
    function ($scope, $timeout, Restangular, OrderService) {

    /*
    Stap1: functie Init
    --------------------
    */
    var init = function() {
        console.log('OrderController loaded');

        clock();

        requestOrdersFromApiLoop();
    }


    /*
    Stap2: Controller vars (niet in scope!)
    ------------------------------------------
    */

    /*
    Stap3: Controller functions (niet in scope!)
    --------------------------------------------
    */
    var requestOrdersFromApi = function() {
        Restangular.all('orders').getList()
        .then(function onSuccess(orders) {

            OrderService.setOrderList(orders);
            $scope.orders = OrderService.getOrders();
            
        }, function onFail(data) {

            console.log('fail');

        });
    }

    var apiPutOrder = function(order) {

        var params = {'orderDate': order.order_date, 'status': order.status};

        order.patch(params)
        .then(function onSuccess(data) { 

            OrderService.removeOrder(order);
            $scope.orders = OrderService.getOrders();
            
        }, function onFail(data) {

            console.log('fail');

        });

    }

    /**
     * Loop orderlist refresh
     */
    var requestOrdersFromApiLoop = function() {

        // eerste keer
        requestOrdersFromApi();

        // dan met teller
        var tickInterval = 30000 //ms

        var tick = function() {
            requestOrdersFromApi();
            $scope.lastUpdate = new Date();
            $timeout(tick, tickInterval); // reset the timer
        } 

        // Start the timer
        $timeout(tick, tickInterval);
    }


    /**
     * Tick Clock
     */
    var clock = function() {
       var tickInterval = 1000 //ms

        var tick = function() {
            $scope.currentTime = new Date() // get the current time
            $timeout(tick, tickInterval); // reset the timer
        } 

        // Start the timer
        $timeout(tick, tickInterval);
    }

    /*
    Stap4: Scope vars
    ------------------
    */
    $scope.currentTime = new Date();
    $scope.lastUpdate = new Date();

    /* Stap5: Scope functions
    -------------------------
    */
    $scope.getTotalPrice = function(order) {
        var price = 0;

        angular.forEach(order.order_details, function(detail) {
            price += detail.amount * detail.price;
        })

        return price;
    }

    $scope.checkDetail = function(detail) {
        detail.done = true;
    }

    $scope.acceptOrder = function(order) {
        order.status = true; 

        apiPutOrder(order);
    }

    /* Stap6: init aanroepen
    --------------------
    */
    init();


}]);