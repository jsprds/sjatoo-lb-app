(function() {

    angular.module('sjatoo').controller('NewOrderController', [
        '$scope',
        '$location',
        '$routeParams',
        'ProductService',
        'ProductCategoriesService',
        'OrderService',
        function ($scope, $location, $routeParams, ProductService, ProductCategoriesService, OrderService) {

        var callback = $routeParams.callback;

        // Constructor
        var init = function() {
            console.log('NewOrderController loaded');


            
            $scope.productCategories = [];
            $scope.products = [];
            loadAllProductCategories(25,0);
            loadAllProducts(25,0);
        };

        // controller functions
        var loadAllProducts = function(limit, offset) {

            if(localStorage.getItem('products')) {
                $scope.products = JSON.parse(localStorage.getItem('products'));
            }

            ProductService.getProductList(limit, offset).then(function(products) {

                if(products.length !== $scope.products.length) {
                    for(var i = 0, l = products.length; i < l; i++) {
                        $scope.products.push(products[i]);
                    }

                    localStorage.setItem('products', JSON.stringify($scope.products));

                    var l = $scope.products.length;

                    // if(l == limit) {
                    //     loadAllUsers(l + 25, l);
                    // }
                }

            },function() {
                console.error('error');
            });
                
        }
        
        var loadAllProductCategories = function(limit, offset) {

            if(localStorage.getItem('productCategories')) {
                $scope.productCategories = JSON.parse(localStorage.getItem('productCategories'));
            }

            ProductCategoriesService.getProductCategoryList(limit, offset).then(function(productCategories) {

                if(productCategories.length !== $scope.productCategories.length) {
                    for(var i = 0, l = productCategories.length; i < l; i++) {
                        $scope.productCategories.push(productCategories[i]);
                    }

                    localStorage.setItem('productCategories', JSON.stringify($scope.productCategories));

                    var l = $scope.productCategories.length;

                    // if(l == limit) {
                    //     loadAllUsers(l + 25, l);
                    // }
                }

            },function() {
                console.error('error');
            });
        }

        var apiSaveOrder = function(order) {

            OrderService.postOrder(order).then(function onSuccess(order) {

                console.log('success');

                if(callback == 'summary') {
                    $location.path('/toog/bestelling/overzicht');
                }
                
            }, function onFail(data) {

                console.log('fail');

            });
        }


        /*
        Stap4: Scope vars
        ------------------
        */

        // ADDTOORDER //
        $scope.order = {
            products: []
        };
        
        /* Stap5: Scope functions
        -------------------------
        */

         // FILTER PRODUCTS LIST //
        $scope.filter = {category: {id: 1}};

        $scope.filterList = function(filter) {
            $scope.filter.category.id = filter;
        }

        // Amount functions
        $scope.riseAmount = function(product) {
            $scope.order.products[product].amount += 1;
        }

        // todo: mag niet kleiner worden als 0 en moet uit lijst bij < 1
        $scope.lowerAmount = function(product) {
            $scope.order.products[product].amount -= 1;

            if($scope.order.products[product].amount < 1) {
                $scope.order.products.splice(product, 1);
            }
        }

        // add product to order Object
        $scope.addToOrder = function(product) {
            //console.log(product);

            var indexOfProduct = productInArray(product, $scope.order.products);
            if(indexOfProduct >= 0) {
                    $scope.order.products[indexOfProduct].amount += 1;
            }
            else {
                $scope.order.products.push({product: product, amount: 1});
            }       
        }

        $scope.confirmOrder = function(products) {

            var order = {};
            order.orderDate = new Date();
            order.orderDetails = [];

            for(var i = 0; i < products.length; i++){
                order.orderDetails[i] = {};
                order.orderDetails[i].product = products[i].product.id;
                order.orderDetails[i].amount = products[i].amount;
                order.orderDetails[i].price = products[i].product.price;
            }

            apiSaveOrder(order);

            $scope.order = {
                products: []
            };
        }

        // misschien beter op te lossen met underscore.js --> Nakijken
        var productInArray = function(search, array) {
            for(var i = 0; i < array.length; i++) {

                // abstracter te maken
               if(array[i].product.id === search.id) {
                    return i;
               }
            }
            return -1;
        }
      
        /* Stap6: init aanroepen
        --------------------
        */
        // start deze motherfucking controller! BAM!
        init();

    }]);

})();