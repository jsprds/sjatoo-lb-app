(function() {

    angular.module('sjatoo').controller('OrderListController', [
        '$scope',
        '$timeout',
        'OrderService',
        function ($scope, $timeout, OrderService) {

        /*
    Stap1: functie Init
    --------------------
    */
    var init = function() {
        console.log('OrderController loaded');

        clock();

        $scope.orders = [];
        requestOrdersFromApiLoop(25,0);
    }


    /*
    Stap2: Controller vars (niet in scope!)
    ------------------------------------------
    */

    /*
    Stap3: Controller functions (niet in scope!)
    --------------------------------------------
    */
    var requestOrdersFromApi = function(limit, offset) {

        OrderService.getOrderList(limit, offset).then(function(orders) {

            for(var i = 0, l = orders.length; i < l; i++) {
                $scope.orders.push(orders[i]);
            }

            var l = $scope.orders.length;

                // if(l == limit) {
                //     loadAllUsers(l + 25, l);
                // }

        },function() {
            console.error('error');
        });
    }

    var apiPutOrder = function(order) {

        var params = {'orderDate': order.orderDate, 'status': order.status};

        OrderService.putOrder(order.id, params)
        .then(function onSuccess(data) { 

            var index = $scope.orders.indexOf(order);

            if (index > -1) {
                $scope.orders.splice(index, 1);
            }
            
        }, function onFail(data) {

            console.log('fail');

        });

    }

    /**
     * Loop orderlist refresh
     */
    var requestOrdersFromApiLoop = function() {

        // eerste keer
        requestOrdersFromApi();

        // dan met teller
        var tickInterval = 30000 //ms

        var tick = function() {
            requestOrdersFromApi();
            $scope.lastUpdate = new Date();
            $timeout(tick, tickInterval); // reset the timer
        } 

        // Start the timer
        $timeout(tick, tickInterval);
    }


    /**
     * Tick Clock
     */
    var clock = function() {
       var tickInterval = 1000 //ms

        var tick = function() {
            $scope.currentTime = new Date() // get the current time
            $timeout(tick, tickInterval); // reset the timer
        } 

        // Start the timer
        $timeout(tick, tickInterval);
    }

    /*
    Stap4: Scope vars
    ------------------
    */
    $scope.currentTime = new Date();
    $scope.lastUpdate = new Date();

    /* Stap5: Scope functions
    -------------------------
    */
    $scope.getTotalPrice = function(order) {
        var price = 0;

        angular.forEach(order.order_details, function(detail) {
            price += detail.amount * detail.price;
        })

        return price;
    }

    $scope.checkDetail = function(detail) {
        detail.done = true;
    }

    $scope.acceptOrder = function(order) {
        order.status = true; 

        apiPutOrder(order);
    }

    /* Stap6: init aanroepen
    --------------------
    */
    init();

    }]);

})();