App.service('EventService', [
    '$resource',
    function ($resource) {

    // Event object to save Event-data
    var events = [];
    var selectedEvent;

    var service = $resource(App.serviceUrl + 'events/:id/:date', null, {
            getEventByDate: {
                method: "GET",
                params: {
                    date: '@date',
                    mail: "a931e359a9b70f8e946bd80d44a11de9ba97d9c1",
                    password: "6f300d897719dcb374649a8548f23630690cae06",
                }
            },
            checkIn: {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                params: {
                    module: "event", //staan vast
                    action: "checkIn", //staat vast
                }
            },
            addEvent: {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                params: {
                    module: "event", //staan vast
                    action: "newEvent", //staat vast
                }
            },
            //je kan hier dan nog andere functies definieren in je service, bijvoorbeeld:
            getAllEvents: {
                method: "GET",
                params: {
                    mail: "a931e359a9b70f8e946bd80d44a11de9ba97d9c1",
                    password: "6f300d897719dcb374649a8548f23630690cae06",
                }
            }
        }
    );

    var getCurrentEvents = function() {
        return events;
    }

    var addCurrentEvent = function(event) {
        if(events.indexOf(event) == -1) {
            events[events.length] = event;
        }
        else {
            console.error('Dit event bestaat al!');
        }
    }

    var getSelectedEvent = function() {
        return selectedEvent;
    }

    var setSelectedEvent = function(event) {
        selectedEvent = event;
    }

    /*
    Zet hier alle functies die je toegangkelijk wilt maken in de controller waarin je deze service zet.
    Je kan hier immers ook helper functies definieren die je intern (in deze service) wil gebruiken (bijvoorbeeld functie om steeds eerste letter van de naam in hoofdletter te zetten), maar niet vanuit de controller aanspreekbaar mogen zijn.
    Enkel wat in API zit, wordt eigenlijk doorgegeven aan de controller.
    */
    var API = {
        getEventByDate: service.getEventByDate,
        checkIn: service.checkIn,
        getAllEvents: service.getAllEvents,
        addEvent: service.addEvent,
        getCurrentEvents: getCurrentEvents,
        addCurrentEvent: addCurrentEvent,
        getSelectedEvent: getSelectedEvent,
        setSelectedEvent: setSelectedEvent
    };

    return API;
}]);