App.service('MemberService', [
    '$resource',
    function ($resource) {

    // user object to save user-data
    var currentMember = {};
    var memberCollection = [];

    var service = $resource(App.serviceUrl + 'members/:action/:id/:date', null, {
            getMemberByRfid: {
                method: "GET",
                params: {
                    id: '@id',
                    action: 'rfid',
                    mail: "a931e359a9b70f8e946bd80d44a11de9ba97d9c1",
                    password: "6f300d897719dcb374649a8548f23630690cae06",
                }
            },
            getMemberByDateOfBirth: {
                method: "GET",
                params: {
                    date: '@date',
                    action: 'dateofbirth',
                    mail: "a931e359a9b70f8e946bd80d44a11de9ba97d9c1",
                    password: "6f300d897719dcb374649a8548f23630690cae06",
                }
            },
            updateMember: {
                method: "PUT",
                params: {
                    mail: "a931e359a9b70f8e946bd80d44a11de9ba97d9c1",
                    password: "6f300d897719dcb374649a8548f23630690cae06",
                }
            },
            insertMember: {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                params: {
                    mail: "a931e359a9b70f8e946bd80d44a11de9ba97d9c1",
                    password: "6f300d897719dcb374649a8548f23630690cae06",
                }
            },
            getAllMembers: {
                method: "GET",
                params: {
                    mail: "a931e359a9b70f8e946bd80d44a11de9ba97d9c1",
                    password: "6f300d897719dcb374649a8548f23630690cae06",
                }
            }
        }
    );

    /* Funtie voor het berekenen van de leeftijd */
    var getAge = function() { 
        var d = new Date(this.dateOfBirth); // this = member object (scope)
        var now = new Date();
        return now.getFullYear() - d.getFullYear();
    };

    var setCurrentMember = function(member) {
        currentMember = member;
        currentMember.getAge = getAge;
    }

    var getCurrentMember = function() {
        return currentMember;
    }
    
    /*
    Zet hier alle functies die je toegangkelijk wilt maken in de controller waarin je deze service zet.
    Je kan hier immers ook helper functies definieren die je intern (in deze service) wil gebruiken (bijvoorbeeld functie om steeds eerste letter van de naam in hoofdletter te zetten), maar niet vanuit de controller aanspreekbaar mogen zijn.
    Enkel wat in API zit, wordt eigenlijk doorgegeven aan de controller.
    */
    var API = {
        getMemberByRfid: service.getMemberByRfid,
        getMemberByDateOfBirth: service.getMemberByDateOfBirth,
        updateMember: service.updateMember,
        getAllMembers: service.getAllMembers,
        insertMember: service.insertMember,
        setCurrentMember: setCurrentMember,
        getCurrentMember: getCurrentMember,
    };

    return API;
}]);