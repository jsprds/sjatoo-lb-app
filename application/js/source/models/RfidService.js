App.service('RfidService',
    function ($q) {

    // listen to rfid reader
    var listenToRfidReader = function() {

        console.log('Start listening to RFID reader');

        var deferred = $q.defer();
        var card = '';

        $(document).keypress(function(e) {
            card += String.fromCharCode(e.which);
               
            if(card.length == 10) {
                console.log('Stop listening to RFID reader');
                deferred.resolve(card);
                card = '';
            }
        });    

        return deferred.promise;
    }

    /*
    Zet hier alle functies die je toegangkelijk wilt maken in de controller waarin je deze service zet.
    Je kan hier immers ook helper functies definieren die je intern (in deze service) wil gebruiken (bijvoorbeeld functie om steeds eerste letter van de naam in hoofdletter te zetten), maar niet vanuit de controller aanspreekbaar mogen zijn.
    Enkel wat in API zit, wordt eigenlijk doorgegeven aan de controller.
    */
    var API = {
        listenToRfidReader: listenToRfidReader
    };

    return API;
});