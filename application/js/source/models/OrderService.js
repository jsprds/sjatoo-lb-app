App.service('OrderService', function() {
    var orderList = [];

    var setOrderList = function(orderArray) {
        orderList = orderArray;
    }

    var addOrder = function(newObj) {
        orderList.push(newObj);
    }

    var removeOrder = function(order) {
        var index = orderList.indexOf(order);
        orderList.splice(index, 1);

        return orderList;
    }

    var getOrders = function(){
        return orderList;
    }

    return {
        setOrderList: setOrderList,
        addOrder: addOrder,
        getOrders: getOrders,
        removeOrder: removeOrder
    };

});