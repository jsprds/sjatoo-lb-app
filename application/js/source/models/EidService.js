App.service('EidService', [
    '$resource',
    function ($resource) {

    // EIDserver draait localhost
    var serviceUrl = 'http://localhost:12345';

    var service = $resource(
        //deze resource maakt gebruik van onze url en voegt een controller (user) en een action (getuser, getalluser) toe.
        // op deze manier kan je je urls makkelijk en dynamisch opbouwen
        serviceUrl, { }, {
            getCard: {
                method: "GET"
            }
        }
    );

    /*
    Zet hier alle functies die je toegangkelijk wilt maken in de controller waarin je deze service zet.
    Je kan hier immers ook helper functies definieren die je intern (in deze service) wil gebruiken (bijvoorbeeld functie om steeds eerste letter van de naam in hoofdletter te zetten), maar niet vanuit de controller aanspreekbaar mogen zijn.
    Enkel wat in API zit, wordt eigenlijk doorgegeven aan de controller.
    */
    var API = {
        getCard: service.getCard,
        getStatus: service.getStatus
    };

    return API;
}]);