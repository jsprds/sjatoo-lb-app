// filters 
angular.module('sjatoo').filter('badDateToISO', function() {
  return function(input) {
  	if(input == null){ return ""; } 
    var goodTime = input.replace(/(.+) (.+)/, "$1T$2Z");
    return goodTime;
  };
})

angular.module('sjatoo').filter('ageFilter', function() {
     function calculateAge(birthday) { // birthday is a date

     	var today = new Date();
        var birthDate = new Date(birthday);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
        
     }

     return function(birthdate) { 
           return calculateAge(birthdate);
     }; 
});