(function(){

    angular.module('sjatoo').factory('OrderService', [
        '$http',
        function($http) {

            // private
            var url = 'http://localhost/sjatoo-lb-backend/web/app_dev.php/api/v1/orders';

            var postOrder = function(order) {

                console.info('Post Order: ', order);

                url += '.json'

                return $http.post(url, order).then(function(response) {
                    
                    return response;

                }, function(error) {

                    return error;

                });

            };

            var putOrder = function(id, params) {

                console.info('Put Order: ', id, params);

                console.log(url);

                url = 'http://localhost/sjatoo-lb-backend/web/app_dev.php/api/v1/orders/' + id + '.json';

                console.log(url);

                return $http.put(url, params).then(function(response) {
                    
                    return response;

                }, function(error) {

                    return error;

                });

            };

            var getOrderList = function(limit, offset) {

                limit = limit | 25;
                offset = offset | 0;

                console.info('Loading orders, limit: ' + limit + ' offset: ' + offset);

                url += '.json?limit=' + limit + '&offset=' + offset;

                return $http.get(url, {limit: limit, offset: offset}).then(function(response) {
                    
                    var orders = [];

                    for(var i = 0, l = response.data.length; i < l; i++) {

                        //var user = new User();

                        orders.push(response.data[i]);
                    }

                    return orders;

                }, function(error) {
                    console.log(error);
                });

            };

            // public endpoints
            return {
                postOrder: postOrder,
                getOrderList: getOrderList,
                putOrder: putOrder,
            };
        }
    ]);

})()