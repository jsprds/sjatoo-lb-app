App.service('EventService', [
    function () {

    var eventList = [];

    var setEventList = function(eventArray) {
        console.log('EventService: reset eventList')
        eventList = eventArray;
    }

    var addEvent = function(newObj) {

        // kijk eerst of we het event al niet bestaat
        for(var i = 0; i < eventList.length; i++) {
            if(eventList[i].id == newObj.id) {
                eventList[i] = newObj;
                return;
            }
        }

        eventList.push(newObj);
        return;
    }

    var removeEvent = function(event) {
        var index = eventList.indexOf(event);
        eventList.splice(index, 1);

        return eventList;
    }

    var getEvents = function(){
        return eventList;
    }

    var getEvent = function(id) {

        for(var i = 0; i < eventList.length; i++) {
            if(eventList[i].id == id) {
                return eventList[i];
            }
        }
        
        return undefined;
    }

    // Lijst de events op die momenteel bezig zijn
    var getCurrentEvents = function() {
        var currentEvents = [];
        var j = 0;

        for(var i = 0; i < eventList.length; i++) {
            if(new Date(eventList[i].startDate) < new Date) {
                currentEvents[j] = eventList[i];
                j++;
            }
        }
        return currentEvents;
    }

    return {
        setEventList: setEventList,
        addEvent: addEvent,
        getEvents: getEvents,
        removeEvent: removeEvent,
        getCurrentEvents: getCurrentEvents
    };

}]);