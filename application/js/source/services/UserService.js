(function(){

    angular.module('sjatoo').factory('UserService', [
        '$http',
        function($http) {

            // private
            var url = 'http://sjatoo.be/api/web/api/v1/users.json';

            var getMemberList = function(limit, offset) {

                limit = limit | 25;
                offset = offset | 0;

                console.info('Loading users, limit: ' + limit + ' offset: ' + offset);

                url += '?limit=' + limit + '&offset=' + offset;

                return $http.get(url, {limit: limit, offset: offset}).then(function(response) {
                    
                    var users = [];

                    for(var i = 0, l = response.data.length; i < l; i++) {

                        //var user = new User();

                        users.push(response.data[i]);
                    }

                    return users;

                }, function(error) {
                    console.log(error);
                });

            };

            // public endpoints
            return {
                getMemberList: getMemberList
            };
        }
    ]);

})()