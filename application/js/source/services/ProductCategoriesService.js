(function(){

    angular.module('sjatoo').factory('ProductCategoriesService', [
        '$http',
        function($http) {

            // private
            var url = 'http://localhost/sjatoo-lb-backend/web/app_dev.php/api/v1/productcategories.json';

            var getProductCategoryList = function(limit, offset) {

                limit = limit | 25;
                offset = offset | 0;

                console.info('Loading product categories, limit: ' + limit + ' offset: ' + offset);

                url += '?limit=' + limit + '&offset=' + offset;

                return $http.get(url, {limit: limit, offset: offset}).then(function(response) {
                    
                    console.log(response);

                    var productCategories = [];

                    for(var i = 0, l = response.data.length; i < l; i++) {

                        //var user = new Product();

                        productCategories.push(response.data[i]);
                    }

                    return productCategories;

                }, function(error) {
                    console.log(error);
                });

            };

            // public endpoints
            return {
                getProductCategoryList: getProductCategoryList
            };
        }
    ]);

})()