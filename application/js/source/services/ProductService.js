(function(){

    angular.module('sjatoo').factory('ProductService', [
        '$http',
        function($http) {

            // private
            var url = 'http://localhost/sjatoo-lb-backend/web/app_dev.php/api/v1/products.json';

            var getProductList = function(limit, offset) {

                limit = limit | 25;
                offset = offset | 0;

                console.info('Loading products, limit: ' + limit + ' offset: ' + offset);

                url += '?limit=' + limit + '&offset=' + offset;

                return $http.get(url, {limit: limit, offset: offset}).then(function(response) {
                    
                    console.log(response);

                    var products = [];

                    for(var i = 0, l = response.data.length; i < l; i++) {

                        //var user = new Product();

                        products.push(response.data[i]);
                    }

                    return products;

                }, function(error) {
                    console.log(error);
                });

            };

            // public endpoints
            return {
                getProductList: getProductList
            };
        }
    ]);

})()