module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		/* Compass: handles sass compilation */
		compass: {
            dev: {
                options: {
                    config: 'compass_dev.rb',
                    force: true
                }
            },
            production: {
                options: {
                    config: 'compass_production.rb',
                    force: true
                }
            }
        },

        /* runs a server */
        connect: {
            options: {
                port: 9000,
                livereload: 35729,
                hostname: 'localhost',
                //hostname: '192.168.1.18',
                base: 'application'
            },
            livereload: {
                options: {
                    open: true,
                }
            },
        },

        /* watch all files */
        watch: {
            sass: {
                files: ['application/**/*.scss'],
                tasks: ['compass:dev']
            },
            /* watch our files for change, reload */
            livereload: {
                files: ['application/**/*.html', 'application**/*.css', 'application/images/*', 'application/**/*.js'],
                options: {
                    livereload: '<%= connect.options.livereload %>'
                }
            },
        },

        uglify: {
            my_target: {
                files: {
                    'dest/output.min.js': ['src/input1.js', 'src/input2.js']
                }
            },
            build: {
                options: {
                    mangle: false
                },
                files: {
                    'build/application.js': [ 'build/**/*.js' ]
                }
            }
        },

        copy: {
            build: {
                cwd: 'application',
                src: [ '**' ],
                dest: 'Build',
                expand: true
            },
        },

        clean: {
            build: {
                src: [ 'build' ]
            },
            stylesheets: {
                src: [ 'build/**/*.scss', '!build/**/*.css' ]
            },
        }
    });

	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.registerTask('default',['connect', 'watch']);
	grunt.registerTask('build', 'Compiles all of the assets and copies the files to the build directory.', [
		'clean',
		'copy',
		'compass:production',
		'clean:stylesheets',
		'uglify',
	]);
}